

//buttons background oncklick
var menuColor = document.querySelectorAll('.event');
 for(var i = 0; i< menuColor.length; i++) {
    menuColor[i].addEventListener('click', function(ChangeColor){
        for(var x = 0; x<menuColor.length; x++) {
            if(menuColor[x] == event.currentTarget) {
                menuColor[x].style.backgroundColor="#7beec7";
            } else {
                menuColor[x].style.backgroundColor="white";
            }
        }
    })
}
// sidebar buattons

var firstBtn= document.querySelector(".one");
var secondBtn= document.querySelector(".two");
var thirdBtn= document.querySelector(".three");
var foursBtn= document.querySelector(".four");
var fivesBtn= document.querySelector(".five");



var sidebarColor = document.querySelectorAll('.sidebar__button');
 for(var i = 0; i< sidebarColor.length; i++) {
    sidebarColor[i].addEventListener('click', function(sideColor){
        for(var x = 0; x<sidebarColor.length; x++) {
            if(sidebarColor[x] == event.currentTarget) {
                sidebarColor[x].style.backgroundColor="white";
            } else {
                sidebarColor[x].style.backgroundColor="gray";
            }
        }
    })
}

var firstScreen = document.querySelector(".preload");
firstBtn.addEventListener('click',showFirstScreen);
function showFirstScreen (){
    firstScreen.scrollIntoView();
    firstScreen.scrollIntoView(false);
    firstScreen.scrollIntoView({block: "end"});
    firstScreen.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
}

var secondScreen = document.querySelector(".navbar");
secondBtn.addEventListener('click',showSecondScreen);
function showSecondScreen (){
    secondScreen.scrollIntoView();
    secondScreen.scrollIntoView(true);
    secondScreen.scrollIntoView({block: "start"});
    secondScreen.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
}

var thirdScreen = document.querySelector(".first--item");
thirdBtn.addEventListener('click',showThirdScreen);
function showThirdScreen (){
    thirdScreen.scrollIntoView();
    thirdScreen.scrollIntoView(true);
    thirdScreen.scrollIntoView({block: "center"});
    thirdScreen.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
}

var foursScreen = document.querySelector(".second--item");
foursBtn.addEventListener('click',showFoursScreen);
function showFoursScreen (){
    foursScreen.scrollIntoView();
    foursScreen.scrollIntoView(true);
    foursScreen.scrollIntoView({block: "center"});
    foursScreen.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
}

var fivesScreen = document.querySelector(".newslatter");
fivesBtn.addEventListener('click',showFivesScreen);
function showFivesScreen (){
    fivesScreen.scrollIntoView();
    fivesScreen.scrollIntoView(true);
    fivesScreen.scrollIntoView({block: "center"});
    fivesScreen.scrollIntoView({behavior: "instant", block: "end", inline: "nearest"});
}

//Portfolio filter

var webBtn= document.querySelector(".button--web");
var logoBtn= document.querySelector(".button--logo");
var graphicBtn= document.querySelector(".button--graphic");
var adverBtn= document.querySelector(".button--advertising");
var fashionBtn= document.querySelector(".button--fashion");
var allBtn= document.querySelector(".button--all");


var webImg=document.querySelector(".image--web");
var logoImg=document.querySelector(".image--logo");
var graphicImg=document.querySelector(".image--graphic");
var adverImg=document.querySelector(".image--advertising");
var graphicImg=document.querySelector(".image--fashion");
var allImg=document.querySelector(".image");

var Wi =document.querySelectorAll(".image--web")
var Li =document.querySelectorAll(".image--logo")
var Gi =document.querySelectorAll(".image--graphic")
var Adi =document.querySelectorAll(".image--advertising")
var Fi =document.querySelectorAll(".image--fashion")
var Ai =document.querySelectorAll(".image")

webBtn.addEventListener('click', ShowWebItems);
function ShowWebItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="none";
        
    }
    for(var i=0;i<Wi.length;i++){
        Wi[i].style.display="block";    
    }
}
logoBtn.addEventListener('click', ShowLogoItems);
function ShowLogoItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="none";

    }
    for(var i=0;i<Li.length;i++){
        Li[i].style.display="block";
    }
}
graphicBtn.addEventListener('click', ShowGraphicItems);
function ShowGraphicItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="none";

    }
    for(var i=0;i<Gi.length;i++){
        Gi[i].style.display="block";
    }
}
adverBtn.addEventListener('click', ShowAdverItems);
function ShowAdverItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="none";

    }
    for(var i=0;i<Adi.length;i++){
        Adi[i].style.display="block";
    }
}
fashionBtn.addEventListener('click', ShowFashionItems);
function ShowFashionItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="none";
    }
    for(var i=0;i<Fi.length;i++){
        Fi[i].style.display="block";
    }
}
allBtn.addEventListener('click', ShowAllItems);
function ShowAllItems () {   
    for(var i=0;i<Ai.length;i++){
        Ai[i].style.display="block";
    }
}

//Section header 

document.addEventListener('scroll',showNav);
 var navShow =document.querySelector(".navbar");
function showNav(){
    if(window.pageYOffset>=300){
       navShow.style.opacity="1";
       navShow.style.width="100%";
    }
    else if(window.pageYOffset>=200){
        navShow.style.opacity="0.5";
        navShow.style.width="50%";
     }
    else{
        navShow.style.opacity="0";
        navShow.style.width="0";
    }
} 

document.addEventListener('scroll',showHeader);
 var menuShow =document.querySelector(".header__title");
function showHeader(){
    if(window.pageYOffset>=700){
        menuShow.style.opacity="1";
        menuShow.style.height="auto";
        menuShow.style.width="44%";
    }
    else if(window.pageYOffset>=500){
        menuShow.style.opacity="0.5";
        menuShow.style.width="22%";
    }
    else{
        menuShow.style.opacity="0";
    }
} 

document.addEventListener('scroll',showHeaderAbout);
var historyImg =document.querySelector('.header__about');   
var historyShow =document.querySelector(".header__content");
function showHeaderAbout(){
    if(window.pageYOffset>=1100){
        historyShow.style.opacity="1";
        historyImg.style.width="44%";
        historyImg.style.height="auto";
    }
    else if(window.pageYOffset>=900){
        historyShow.style.opacity="0.5";       
    }
    else{
        historyShow.style.opacity="0";
        historyImg.style.width="0";
        historyImg.style.height="0";
    }
}

// Section aboutUs

document.addEventListener('scroll',showAboutUs); 
var aboutShow =document.querySelector(".aboutus__title");
var aboutTextShow =document.querySelector(".aboutus__maintitle");
function showAboutUs(){
    if(window.pageYOffset>=1500){
        aboutShow.style.opacity="1";
        aboutShow.style.width="70%";
        aboutTextShow.style.fontSize="3rem"
    }
    else if(window.pageYOffset>=1600){
        aboutShow.style.opacity="0.5"; 
        aboutShow.style.width="35%"; 
        aboutTextShow.style.fontSize="2rem"     
    }
    else{
        aboutShow.style.opacity="0";
        aboutShow.style.width="0";
        aboutTextShow.style.fontSize="1rem"  
    }
}

document.addEventListener('scroll',showAboutUsContent); 
var aboutContentShow =document.querySelector(".aboutus__content");
var aboutImgShow =document.querySelector(".aboutus__image");
function showAboutUsContent(){
    if(window.pageYOffset>=1700){
        if(window.innerWidth<=1160){
            aboutContentShow.style.opacity="1";
            aboutImgShow.style.width="60%";            
        }
        else{
            aboutContentShow.style.opacity="1";
            aboutImgShow.style.width="40%";   
        }
    }
    else if(window.pageYOffset>=1600){
        aboutContentShow.style.opacity="0.5"; 
        aboutImgShow.style.width="20%";    
    }   
    else{
        aboutContentShow.style.opacity="0";
        aboutImgShow.style.width="0"; 
    }
}

//Section gallery

document.addEventListener('scroll',showGallery); 
var galleryShow =document.querySelector(".gallery__title");
var galleryTextShow =document.querySelector(".gallery__maintitle");
function showGallery(){
    if(window.pageYOffset>=2500){
        galleryShow.style.opacity="1";
        galleryShow.style.width="70%";
        galleryTextShow.style.fontSize="3rem"
    }
    else if(window.pageYOffset>=2300){
        galleryShow.style.opacity="0.5"; 
        galleryShow.style.width="35%"; 
        galleryTextShow.style.fontSize="2rem" ;    
    }
    else{
        galleryShow.style.opacity="0";
        galleryShow.style.width="0";
        galleryTextShow.style.fontSize="1rem";  
    }
}

document.addEventListener('scroll',showGalleryImg); 
var galleryMenuShow =document.querySelector(".gallery__menu");
var galleryImgShow =document.querySelector(".gallery__images");
function showGalleryImg(){
    if(window.pageYOffset>=2800){
        if(window.innerWidth<=1160){
            galleryMenuShow.style.opacity="1";
            galleryImgShow.style.width="100%";
            galleryImgShow.style.height="100rem";
            galleryImgShow.style.opacity="1";
        }
        else{
            galleryMenuShow.style.opacity="1";
            galleryImgShow.style.width="80%"
            galleryImgShow.style.height="100rem";
            galleryImgShow.style.opacity="1";
        }
    }
    else if(window.pageYOffset>=2700){
        galleryMenuShow.style.opacity="1";
        galleryImgShow.style.width="40%"
        galleryImgShow.style.height="50rem";
        galleryImgShow.style.opacity="0.5";   
    }
    else{
        galleryMenuShow.style.opacity="0";
        galleryImgShow.style.width="0" ;
        galleryImgShow.style.height="0"; 
        galleryImgShow.style.opacity="0";
    }
}

//section newslatter

document.addEventListener('scroll',showNewsImg); 
var newslatterTitleShow =document.querySelector(".title");
var newslatterFormShow =document.querySelector(".form");
function showNewsImg(){
    if(window.pageYOffset>=3500){
        if(window.innerWidth<=790){
            newslatterTitleShow.style.opacity="1";
            newslatterTitleShow.style.width="90%";
            newslatterFormShow.style.width="90%";
            newslatterFormShow.style.opacity="1";
            newslatterFormShow.style.marginLeft="0rem";
        }
        else{
            newslatterTitleShow.style.opacity="1";
            newslatterTitleShow.style.width="50%";
            newslatterFormShow.style.width="50%";
            newslatterFormShow.style.opacity="1";
            newslatterFormShow.style.marginLeft="2.5rem";
        }
    }
    else if(window.pageYOffset>=3100){
        newslatterTitleShow.style.opacity="0.5";
        newslatterTitleShow.style.width="0";
        newslatterFormShow.style.width="0";
        newslatterFormShow.style.opacity="0.0";
        newslatterFormShow.style.marginLeft="98%";
    }
    else{
        newslatterTitleShow.style.opacity="0";
        newslatterTitleShow.style.width="0";
        newslatterFormShow.style.width="0";
        newslatterFormShow.style.opacity="0"; 
        newslatterFormShow.style.marginLeft="2.5rem";
    }
}

//section footer

document.addEventListener('scroll',showFooterImg); 
var footerShow =document.querySelector(".footer__content");
function showFooterImg(){
    if(window.pageYOffset>=3900){
        footerShow.style.opacity="1";
    }
    else(window.pageYOffset>=3800)
        footerShow.style.opacity="0.5";
}





